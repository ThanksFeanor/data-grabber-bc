#-*- coding: UTF-8 -*-
import csv
import os
from BCHelpers import pathtofile, ENJPfolders

def readExplanation(fp,splitter,names):
    with open(fp, 'r', encoding="utf-8") as csvfile:
          reader = csv.reader(csvfile, delimiter=splitter)
          rowstorage = ""
          for row in reader: #For each form of the unit
              if len(row) > 0:
                  if rowstorage != row[0]: #Don't record name that is same as previous form (e.g. for no TF)
                      names.append(row[0])
                  else:
                      names.append("")
                  rowstorage = row[0] 
                      
                     
def createEnemyNames():
    (EN,JP) = ENJPfolders()
    
    resLocal_EN = os.path.join(EN,"resLocal")
    resLocal_JP = os.path.join(JP,"resLocal")

    namesEN = []
    with open(pathtofile(resLocal_EN,'Enemyname.tsv'), 'r', encoding='utf-8') as csvfile:
        reader = csv.reader(csvfile)
        for row in reader:
            namesEN.append(row)
            
    namesJP = []
    with open(pathtofile(resLocal_JP,'Enemyname.tsv'), 'r', encoding='utf-8') as csvfile:
        reader = csv.reader(csvfile)
        for row in reader:
            namesJP.append(row)
            
    namesExtra = []
    with open(pathtofile(os.path.dirname(JP),'tunit_extras.csv'), 'r') as csvfile:
        reader = csv.reader(csvfile)
        for row in reader:
            namesExtra.append(row)
            
            
    names = [""]*len(namesJP)
    for i in range(len(namesJP)):
        if len(namesEN) > i and len(namesEN[i]) > 0:
            names[i] = namesEN[i]
        elif len(namesExtra) > i and len(namesExtra[i]) > 0:
            names[i] = namesExtra[i]
        elif len(namesJP[i]) > 0 and not namesJP[i] == ['ダミー']:
            names[i] = namesJP[i]

    with open('_EnemynameOutput.csv', 'w', newline='', encoding="utf-8") as output:
        writer = csv.writer(output)
        for name in names:
            writer.writerow(name)
        
    return names
            

# def createUnitNames_OLD():
    
#     contents = os.listdir(os.path.join(os.path.dirname(os.getcwd()),"resLocal"))
#     resLocal = os.path.join(os.path.dirname(os.getcwd()),"resLocal")
#     onlyfiles = [f for f in contents if os.path.isfile(os.path.join(resLocal, f)) and f.endswith("_en.csv") and f.startswith("Unit_Explanation")]
#     nmax = max([int(f[16:][:len(f)-23]) for f in onlyfiles])
    
#     n = 1
#     with open('_UnitExplanationOutput.csv', 'w', newline='', encoding="ISO-8859-1") as output:
#         writer = csv.writer(output)
#         while n > 0:
            
#             names = [n]
            
#             try:
#                 with open(pathtofile('resLocal','Unit_Explanation'+str(n)+'_en.csv'), 'r', encoding="ISO-8859-1") as csvfile:
#                       reader = csv.reader(csvfile, delimiter='|')
#                       rowstorage = ""
#                       for row in reader: #For each form of the unit
#                           if len(row) > 0:
#                               if rowstorage != row[0]: #Don't record name that is same as previous form (e.g. for no TF)
#                                   names.append(row[0])
#                               else:
#                                   names.append("")
#                               rowstorage = row[0] 
#             except IOError:
#                 print(str(n)+" not found")
            
#             writer.writerow(names)
#             if n == nmax:
#                 n = -1
#             n = n + 1
            
# def fillJPNames():
#     enames = []
#     with open('_UnitExplanationOutput.csv', 'r', encoding='utf-8') as csvfile:
#       reader = csv.reader(csvfile)
#       for row in reader:
#           try:
#               enames.append(row)
#           except IndexError:
#               enames.append("")
              
              
#     contents = os.listdir(os.path.join(os.path.dirname(os.getcwd()),"resLocal"))
#     resLocal = os.path.join(os.path.dirname(os.getcwd()),"resLocal")
#     onlyfiles = [f for f in contents if os.path.isfile(os.path.join(resLocal, f)) and f.endswith("_ja.csv") and f.startswith("Unit_Explanation")]
#     nmax = max([int(f[16:][:len(f)-23]) for f in onlyfiles])
    
#     while len(enames) < nmax:
#         enames.append(["","","",""])
    
#     n = 1
#     with open('_UnitExplanationOutputJP.csv', 'w', newline='', encoding="utf-8") as output:
#         writer = csv.writer(output)
#         while n > 0:
            
#             names = [n]
            
            
#             try:
#                 with open(pathtofile('resLocal','Unit_Explanation'+str(n)+'_ja.csv'), 'r', encoding="utf-8") as csvfile:
#                       reader = csv.reader(csvfile, delimiter=',')
#                       rowstorage = ""
#                       for row in reader: #For each form of the unit
#                           if len(row) > 0:
#                               if rowstorage != row[0]: #Don't record name that is same as previous form (e.g. for no TF)
#                                   names.append(row[0])
#                               else:
#                                   names.append("")
#                               rowstorage = row[0] 
#             except IOError:
#                 print(str(n)+" not found")
            
#             print(n)
#             if len(enames[n-1]) < 2 or enames[n-1][1] == "":
#                 writer.writerow(names)
#             else:
#                 writer.writerow(enames[n-1])
            
            
#             if n == nmax:
#                 n = -1
#             n = n + 1
              