import csv
import copy
import os
from UnitExplanation import createUnitNames
from BCHelpers import pathtofile

def GenerateGatyaSetRCSV():
    gatyas = []
    
    with open(pathtofile('DataLocal','GatyaDataSetR1.csv'), 'r', encoding='utf-8') as csvfile:
          reader = csv.reader(csvfile)
          for row in reader:
              gatyas.append(row)
              
    names = []
    
    if os.path.isfile('_UnitExplanationOutput.csv') == False:
        createUnitNames()
    
    with open('_UnitExplanationOutput.csv', 'r', encoding='utf-8') as csvfile:
          reader = csv.reader(csvfile)
          for row in reader:
              try:
                  names.append(row[1])
              except IndexError:
                  names.append("")
                  
    rarities = []
    with open(pathtofile('DataLocal','unitbuy.csv'), 'r', encoding='utf-8') as csvfile:
          reader = csv.reader(csvfile)
          for row in reader:
              try:
                  rarities.append(int(row[14][0]))
              except IndexError:
                  rarities.append("")
                  
    for n in range(len(gatyas)):
        for i in range(len(gatyas[n])):
              try:
                gatyas[n][i] = int(gatyas[n][i])
              except:
                  gatyas[n][i] = ""
        gatyas[n][:] = [name for name in gatyas[n] if name != "" and name != -1]
             
    
    namedgatyas = copy.deepcopy(gatyas)
    
    for n in range(len(gatyas)):
        for i in range(len(gatyas[n])):
              try:
                namedgatyas[n][i] = (rarities[gatyas[n][i]],names[gatyas[n][i]])
              except:
                  namedgatyas[n][i] = ""
        namedgatyas[n][:] = [t for t in namedgatyas[n] if t[1] != ""]
                 
    for n in range(len(gatyas)):
        namedgatyas[n][:] = [n,
            [t[1] for t in namedgatyas[n] if t[0] == 2],
            [t[1] for t in namedgatyas[n] if t[0] == 3],
            [t[1] for t in namedgatyas[n] if t[0] == 4],
            [t[1] for t in namedgatyas[n] if t[0] == 5]
            ]
        
    with open('gatyasetsR.csv', 'w', newline='', encoding='utf-8') as output:
        writer = csv.writer(output)
        for gatyaset in namedgatyas:
            writer.writerow(gatyaset)
    
    return namedgatyas
            
def GetGatyaSet(n):
    return GenerateGatyaSetCSV()[n]