import os
def pathtofile(loc, file):
    return os.path.join(os.path.dirname(os.getcwd()), loc, file)

def isjp():
    return os.path.basename(os.path.dirname(os.getcwd()))[:2] == "JP"

def ENJPfolders():
    if isjp():
        JP = os.path.dirname(os.getcwd())
        EN = os.path.join(os.path.dirname(os.path.dirname(os.getcwd())),"Latest")
    else:
        EN = os.path.dirname(os.getcwd())
        JP = os.path.join(os.path.dirname(os.path.dirname(os.getcwd())),"JPLatest")
    return (EN,JP)
        