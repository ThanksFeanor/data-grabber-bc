import csv
from BCHelpers import pathtofile, ENJPfolders
import os

(EN,JP) = ENJPfolders()

resLocal_EN = os.path.join(EN,"resLocal")
resLocal_JP = os.path.join(JP,"resLocal")

s_EN = []
s_JP = []

with open(pathtofile(resLocal_EN,'localizable.tsv'), 'r', encoding='utf-8') as csvfile:
      reader = csv.reader(csvfile,delimiter="\t")
      for row in reader:
          if row[0][:len("potential_skill_name")] == "potential_skill_name":
              s_EN.append([int(row[0][-2:]),row[1]])
              
with open(pathtofile(resLocal_JP,'localizable.tsv'), 'r', encoding='utf-8') as csvfile:
      reader = csv.reader(csvfile,delimiter="\t")
      for row in reader:
          if row[0][:len("potential_skill_name")] == "potential_skill_name":
              s_JP.append([int(row[0][-2:]),row[1]])
              
s = [""]*s_JP[-1][0]

for x in s_JP:
    s[x[0]-1] = x[1]
for x in s_EN:
    s[x[0]-1] = x[1]
    
with open('SkillNames.csv', 'w', newline='', encoding="utf-8") as output:
    writer = csv.writer(output)
    for r in s:
        writer.writerow(r)