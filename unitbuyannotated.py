import csv
import os
from UnitExplanation import createUnitNames
from BCHelpers import pathtofile
import subprocess


def annotate():     
    names = []
    
    if os.path.isfile('_UnitExplanationOutput.csv') == False:
        createUnitNames()
    
    with open('_UnitExplanationOutput.csv', 'r', encoding='utf-8') as csvfile:
          reader = csv.reader(csvfile)
          for row in reader:
              try:
                  names.append(row[1])
              except IndexError:
                  names.append("")
                  
    items = []
    
    if os.path.isfile('ItemData.csv') == False:
        subprocess.Popen("ItemData.py", shell=True)
    
    with open('ItemData.csv', 'r', encoding='utf-8') as csvfile:
          reader = csv.reader(csvfile)
          for row in reader:
              try:
                  items.append(row[1])
              except IndexError:
                  items.append("")
                  
    unitbuy = []
    with open(pathtofile('DataLocal','unitbuy.csv'), 'r', encoding='utf-8') as csvfile:
          reader = csv.reader(csvfile)
          for row in reader:
              try:
                  unitbuy.append(row)
              except IndexError:
                  unitbuy.append("")
    
    headers = ["Name", "Unlock Stage"
    ,"Level 0 cost","Level 1 cost","Level 2 cost","Level 3 cost","Level 4 cost","Level 5 cost","Level 6 cost","Level 7 cost","Level 8 cost","Level 9 cost","Level 10 cost" #D-M
    ,"Method to Obtain", "Rarity", "Order ID", "Unlock Chapter", "XP Sell Value", "Gacha Rarity Group" #N-S
    ,"Max Level", "Max + Level", "Level for True Form Unlock", "???", "???" #T-X
    ,"TF Unlock Code for Stage Drop", "Always 0", "Gacha = 30", "Always -1" #Y-AB
    ,"TF XP Cost", "Fruit 1", "Amount", "Fruit 2", "Amount", "Fruit 3", "Amount", "Fruit 4", "Amount", "Fruit 5", "Amount" #AC-AM
    ,"unused","unused","unused","unused","unused","unused","unused","unused","unused","unused","unused" #AN-AX
    ,"Max Level from XP", "Max Level with Catseyes", "Max + Level" #AY-BA
    ,"Size/positiion modifier 1","Size/positiion modifier 2","Size/positiion modifier 3"#BB-BD
    ,"Always 0", "Boostable", "Version Number Added", "NP Sell Value", "TF Unlock Event ID"] #BE-BI
    
    headers = headers + [""]*(len(unitbuy[0]) - len(headers))
    
    annotated = [headers] + unitbuy
    
    for n in range(len(unitbuy)):
        try:
            annotated[n+1] = [names[n]] + annotated[n+1]
        except:
            print(n)
        
    nameFruits(annotated, items)
        
    with open('unitbuy_annotated.csv', 'w', newline='', encoding='utf-8') as output:
        writer = csv.writer(output)
        for row in annotated:
            writer.writerow(row)
    
    return annotated

def nameFruits(annotated, items):
    indices = [annotated[0].index("Fruit "+str(n)) for n in range(1,6)]
    for n in range(1, len(annotated)):
        for index in indices:
            try:
                if int(annotated[n][index]) > 0:
                    annotated[n][index] = items[int(annotated[n][index])]
            except:
                pass