import csv
from BCHelpers import pathtofile, ENJPfolders
import os
from UnitExplanation import createUnitNames

(EN,JP) = ENJPfolders()

resLocal_EN = os.path.join(EN,"resLocal")
DataLocal_EN = os.path.join(EN,"DataLocal")
resLocal_JP = os.path.join(JP,"resLocal")
DataLocal_JP = os.path.join(JP,"DataLocal")

t_JP = []
desc_EN = []
npb2_EN = []
desc_JP = []
npb2_JP = []

with open(pathtofile(DataLocal_JP,'SkillAcquisition.csv'), 'r', encoding='utf-8') as csvfile:
      reader = csv.DictReader(csvfile)
      for row in reader:
          t_JP.append(row)
          
with open(pathtofile(resLocal_EN,'SkillDescriptions.csv'), 'r', encoding='utf-8') as csvfile:
      reader = csv.reader(csvfile, delimiter="|")
      for row in reader:
          desc_EN.append(row)
          
with open(pathtofile(resLocal_EN,'nyankoPictureBook2_en.csv'), 'r', encoding='utf-8') as csvfile:
      reader = csv.reader(csvfile, delimiter="|")
      for row in reader:
          npb2_EN.append(row)
          
with open(pathtofile(resLocal_JP,'SkillDescriptions.csv'), 'r', encoding='utf-8') as csvfile:
      reader = csv.reader(csvfile, delimiter=",")
      for row in reader:
          desc_JP.append(row)
          
with open(pathtofile(resLocal_JP,'nyankoPictureBook2_ja.csv'), 'r', encoding='utf-8') as csvfile:
      reader = csv.reader(csvfile, delimiter=",")
      for row in reader:
          npb2_JP.append(row)
               
names = []

if os.path.isfile('_UnitExplanationOutput.csv') == False:
    createUnitNames()

with open('_UnitExplanationOutput.csv', 'r', encoding='utf-8') as csvfile:
      reader = csv.reader(csvfile)
      for row in reader:
          try:
              names.append(row[1])
          except IndexError:
              names.append("")
              
s_EN = []
s_JP = []

with open(pathtofile(resLocal_EN,'localizable.tsv'), 'r', encoding='utf-8') as csvfile:
      reader = csv.reader(csvfile,delimiter="\t")
      for row in reader:
          if row[0][:len("potential_skill_name")] == "potential_skill_name":
              s_EN.append([int(row[0][-2:]),row[1]])
              
with open(pathtofile(resLocal_JP,'localizable.tsv'), 'r', encoding='utf-8') as csvfile:
      reader = csv.reader(csvfile,delimiter="\t")
      for row in reader:
          if row[0][:len("potential_skill_name")] == "potential_skill_name":
              s_JP.append([int(row[0][-2:]),row[1]])
              
skillnames = [""]*s_JP[-1][0]

for x in s_JP:
    skillnames[x[0]-1] = x[1]
for x in s_EN:
    skillnames[x[0]-1] = x[1]
     
              
desc = [n for n in desc_JP]
desc[:len(desc_EN)] = desc_EN

npb2 = [n for n in npb2_JP]
npb2[:len(npb2_EN)] = npb2_EN

nicknames = []
with open(pathtofile(os.path.dirname(JP),'talentnicknames.txt'), 'r') as csvfile:
    reader = csv.reader(csvfile,delimiter="\t")
    for row in reader:
        nicknames.append(row[0])

for tdict in t_JP:
    tdict["Name"] = names[int(tdict["ID"])]
    tdict["Abilities"] = [
        [int(tdict["abilityID_A"]),skillnames[int(tdict["abilityID_A"])-1]],
        [int(tdict["abilityID_B"]),skillnames[int(tdict["abilityID_B"])-1]],
        [int(tdict["abilityID_C"]),skillnames[int(tdict["abilityID_C"])-1]],
        [int(tdict["abilityID_D"]),skillnames[int(tdict["abilityID_D"])-1]],
        [int(tdict["abilityID_E"]),skillnames[int(tdict["abilityID_E"])-1]]
        ]
    tdict["Descriptions"] = [
        [int(tdict["textID_A"]),nicknames[int(tdict["textID_A"])-1]],
        [int(tdict["textID_B"]),nicknames[int(tdict["textID_B"])-1]],
        [int(tdict["textID_C"]),nicknames[int(tdict["textID_C"])-1]],
        [int(tdict["textID_D"]),nicknames[int(tdict["textID_D"])-1]],
        [int(tdict["textID_E"]),nicknames[int(tdict["textID_E"])-1]]
        ]
    tdict["Levels"] = [
        [int(tdict["LvID_A"]),int(tdict["MAXLv_A"])],
        [int(tdict["LvID_B"]),int(tdict["MAXLv_B"])],
        [int(tdict["LvID_C"]),int(tdict["MAXLv_C"])],
        [int(tdict["LvID_D"]),int(tdict["MAXLv_D"])],
        [int(tdict["LvID_E"]),int(tdict["MAXLv_E"])]
        ]

def tanalysis():
    tanalysis = []
    for cat in t_JP:
        for i in range(5):
            tanalysis.append([cat["Descriptions"][i][0],
                              cat["Descriptions"][i][1],
                              cat["Levels"][i][0],
                              cat["Levels"][i][1],
                              ]
                             )
    
    sorted(tanalysis)
    
    tanalysis2 = {}
    
    for x in tanalysis:
        if x[1] in tanalysis2:
            if x[2] not in tanalysis2[x[1]]["Types"]:
                tanalysis2[x[1]]["Types"].append(x[2])
            if x[3] not in tanalysis2[x[1]]["Max"]:
                tanalysis2[x[1]]["Max"].append(x[3])
        else:
            tanalysis2[x[1]] = {}
            tanalysis2[x[1]]["Types"] = [x[2]]
            tanalysis2[x[1]]["Max"] = [x[3]]
    
    with open('tanalysis.csv', 'w', newline='', encoding="utf-8") as output:
        writer = csv.writer(output)
        for r in sorted(tanalysis2):
            types = sorted(tanalysis2[r]["Types"])
            maxLevel  = max(tanalysis2[r]["Max"][0],1)
            if maxLevel == 5 and types == [2]:
                derivedType = 3
            elif types == [12]:
                derivedType = 3
            elif len(list(set([i%3 for i in types]))) == 1:
                derivedType = list(set([i%3 for i in types]))[0]
            else:
                print("Indeterminate")
            writer.writerow([r, derivedType, maxLevel])

    return tanalysis2